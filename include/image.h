#ifndef IMAGE_H
#define IMAGE_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <memory>
#include <cstdlib>

size_t Fcexif(const std::string sImg);

#endif // IMAGE_H
