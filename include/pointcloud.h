#ifndef POINTCLOUD_H
#define POINTCLOUD_H

#include <algorithm>
#include <vector>
#include <iostream>

using namespace std;

void las2pcd(const std::string& file, const std::string& pcdFile);
vector<double> getMinMax(const std::string& lasFile);
vector<float> getCentroid(const std::string& pcdFile);
vector<float> getNeighbor(const std::string& pcdFile, int x, int y, int z);

#endif // POINTCLOUD_H
