#ifndef PHOTOS_H
#define PHOTOS_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <iostream>


#include "openMVG/numeric/numeric.h"
#include "openMVG/image/image_container.hpp"
#include "openMVG/image/image_converter.hpp"
#include "openMVG/image/pixel_types.hpp"

using namespace std;
using namespace openMVG;
using namespace openMVG::image;

bool isInside(Vec2 point, double width, double height);
vector<float> getPosOri(int id_image, const std::string file_path);
vector<float> getCalibration(const std::string file_path);
double getResolution(int id_image, vector<double> centroid,  const std::string ori_file_path, const std::string cali_file_path);
openMVG::image::RGBColor getRGB_TEMP(int id_image, std::string const image, double x, double y, double z, const std::string cali_file_path, const std::string ori_file_path);
openMVG::image::RGBColor getRGB(int id_image, std::string const image, double x, double y, double z, const std::string cali_file_path, const std::string ori_file_path);



#endif // PHOTOS_H
