#ifndef ORTHO_H
#define ORTHO_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
#include <list>

#include "openMVG/image/image_container.hpp"
#include "openMVG/image/image_converter.hpp"
#include "openMVG/image/pixel_types.hpp"
#include "openMVG/numeric/numeric.h"

using namespace std;
using namespace openMVG;
using namespace openMVG::image;


int mean(std::list<int> liste);
void saveImg(Image<RGBColor> imaRGB, double resolution, vector<double> MinMax);
std::list<std::string> repertoire(std::string Images_File);
void fillOrtho( const std::string Images_File, const std::string& pcdFile, double resolution, vector<double> MinMax, const std::string cali_file_path, const std::string ori_file_path);

#endif // ORTHO_H
