#include "pointcloud.h"

#include <iostream>
#include <vector>
#include <thread>

#include "laswriter.hpp"
#include "lasreader.hpp"

#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>

using namespace std;



void las2pcd(const std::string& lasFile, const std::string& pcdFile)
{
    /*
     * Convertit un fichier .las en fichier .pcd
     * entrée : chemin du fichier .las entré, chemin du fichier .pcd sorti
     * sortie : None
     */

    const std::string& your_pcd_out_file_path = pcdFile;

    LASreadOpener lasreadopener;
    lasreadopener.set_file_name(lasFile.c_str());
    LASreader* lasreader = lasreadopener.open();
    size_t count = lasreader->header.number_of_point_records;
    pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloudPtr(new pcl::PointCloud<pcl::PointXYZ>);
    pointCloudPtr->resize(count);
    pointCloudPtr->width = 1;
    pointCloudPtr->height = count;
    pointCloudPtr->is_dense = false;
    size_t i = 0;
    while(lasreader->read_point() && i < count)
    {
        pointCloudPtr->points[i].x = lasreader->point.get_x();
        pointCloudPtr->points[i].y = lasreader->point.get_y();
        pointCloudPtr->points[i].z = lasreader->point.get_z();
        ++i;
    }
    pcl::io::savePCDFileASCII(your_pcd_out_file_path,*pointCloudPtr);

}

//http://pointclouds.org/documentation/tutorials/moment_of_inertia.php

vector<double> getMinMax(const std::string& pcdFile)
{
    /*
     * Calcule le minimum et maximum des coordonnées en x, y et z du nuage
     * entrée : chemin du fichier .pcd en entrée
     * sortie : vecteur de la forme [Xmax, Ymax, Zmax, Xmin, Ymin, Zmin]
     */

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    if (pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile, *cloud) == -1) //* load the file
      {
        PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
        std::cout << "ERROR chargement"<< std::endl;
      }

    pcl::PointXYZ minPt, maxPt;
    pcl::getMinMax3D (*cloud, minPt, maxPt);

    std::vector<double> retour(6);
    retour[0] = maxPt.x;
    retour[1] = maxPt.y;
    retour[2] = maxPt.z;
    retour[3] = minPt.x;
    retour[4] = minPt.y;
    retour[5] = minPt.z;
    return retour;
}

vector<float> getCentroid(const std::string& pcdFile)
{
    /*
     * Calcule le centroide du nuage
     * entrée : chemin du fichier .pcd en entrée
     * sortie : vecteur de la forme [Xcentroid, Ycentroid, Zcentroid]
     */

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    if (pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile, *cloud) == -1) //* load the file
      {
        PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
        std::cout << "ERROR chargement"<< std::endl;
      }
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid (*cloud, centroid);
    std::vector<float> retour(3);
    retour[0] = centroid[0];
    retour[1] = centroid[1];
    retour[2] = centroid[2];
    return retour;



}

//http://pointclouds.org/documentation/tutorials/kdtree_search.php

vector<float> getNeighbor(const std::string& pcdFile, int x, int y, int z)
{
    /*
     * Détermine le plus proche voisin dans un nuage d'un points 3D
     * entrée : chemin du fichier .pcd entré, coordonnées x, y, z du point 3D
     * sortie : Vecteur de la forme [Xvoisin, Yvoisin, Zvoisin, distance]
     */

    // Initialisation du vecteur renvoyé en cas d'erreur
    std::vector<float> error(4);
    error[0] = -1;
    error[1] = -1;
    error[2] = -1;
    error[3] = -1;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    if (pcl::io::loadPCDFile<pcl::PointXYZ> (pcdFile, *cloud) == -1) //* load the file
      {
        PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
        return error;
      }

      pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
      kdtree.setInputCloud (cloud);

      pcl::PointXYZ searchPoint;

      searchPoint.x = x;
      searchPoint.y = y;
      searchPoint.z = z;

      int K = 1; //nombre de voisins recherché

        std::vector<int> pointIdxNKNSearch(K); //identifiant du voisin
        std::vector<float> pointNKNSquaredDistance(K); // distance au voisin

        if ( kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
          {
            for (std::size_t i = 0; i < pointIdxNKNSearch.size (); ++i)
            {

                pcl::PointXYZ neighbor;
                neighbor.x = cloud->points[ pointIdxNKNSearch[0] ].x;
                neighbor.y = cloud->points[ pointIdxNKNSearch[0] ].y;
                neighbor.z = cloud->points[ pointIdxNKNSearch[0] ].z;

                std::vector<float> retour(4);
                retour[0] = neighbor.x;
                retour[1] = neighbor.y;
                retour[2] = neighbor.z;
                retour[3] = pointNKNSquaredDistance[0];

                return retour;

            }
          }
        cout <<"ERROR find neighbor" << endl;
        return error;
}



