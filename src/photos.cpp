#include "photos.h"

#include <algorithm>
#include <vector>
#include <fstream>
#include <math.h>

#include "openMVG/exif/exif_IO_EasyExif.hpp"
#include "openMVG/exif/exif_IO.hpp"

#include "openMVG/sfm/pipelines/sfm_robust_model_estimation.hpp"
#include "openMVG/sfm/sfm.hpp"

#include "openMVG/multiview/projection.hpp"
#include "openMVG/multiview/triangulation.hpp"
#include "openMVG/multiview/test_data_sets.hpp"
#include "openMVG/multiview/solver_resection_kernel.hpp"
#include "openMVG/multiview/solver_resection_p3p.hpp"
#include "openMVG/multiview/solver_essential_kernel.hpp"
#include "openMVG/multiview/test_data_sets.hpp"

#include "openMVG/robust_estimation/robust_estimator_ACRansacKernelAdaptator.hpp"

#include "openMVG/image/pixel_types.hpp"
#include "openMVG/image/image_io.hpp"
#include "openMVG/image/image_container.hpp"
#include "openMVG/image/image_converter.hpp"
#include "openMVG/cameras/Camera_Pinhole_Radial.hpp"
#include "openMVG/cameras/Camera_Pinhole_Brown.hpp"
#include "openMVG/cameras/Camera_Intrinsics.hpp"
#include "openMVG/cameras/Camera_Intrinsics_io.hpp"
#include "openMVG/cameras/Camera_undistort_image.hpp"

#include "openMVG/numeric/eigen_alias_definition.hpp"
#include "openMVG/numeric/numeric.h"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include <boost/foreach.hpp>

using namespace openMVG;
using namespace openMVG::image;
using namespace openMVG::exif;
using namespace openMVG::cameras;
using namespace openMVG::geometry;
using namespace std;
using namespace sfm;




vector<float> getPosOri(int id_image, const std::string file_path)
{
    /*
     * Lit le fichier de calibration externe d'une caméra, et renvoit les paramètres sous forme de vecteur de flottants.
     * Entrée : identifiant de la caméra (position dans la liste), chemin d'accès au fichier de calibration externe
     * Sortie : vecteur de flottants de taille 6, de la forme [Xcaméra, Ycaméra, Zcaméra, YAWcaméra, PITCHcaméra, ROLLcaméra]
     */
    ifstream inFile;
    int sum = 0;
    std::vector<float> retour(6);

    inFile.open(file_path);

        if (!inFile) {
            cout << "Unable to open file";
            exit(1); // terminate with error
        }

        std::string str;
        while (std::getline(inFile, str)) {
            sum+=1;
            if (sum==id_image)
            {
                typedef std::vector<std::string> Tokens;
                    Tokens tokens;
                    boost::split( tokens, str, boost::is_any_of("\t") );

                    retour[0] = std::stof(tokens[1]);
                    retour[1] = std::stof(tokens[2]);
                    retour[2] = std::stof(tokens[3]);
                    retour[3] = std::stof(tokens[4]);
                    retour[4] = std::stof(tokens[5]);
                    retour[5] = std::stof(tokens[6]);
            }

        }
        inFile.close();
        return retour;
}

vector<float> getCalibration(const std::string file_path)
{
    /*
     * Lit le fichier de calibration interne d'une caméra, et renvoit les paramètres sous forme de vecteur de flottants.
     * Entrée : chemin d'accès au fichier de calibration interne
     * Sortie : vecteur de flottants de taille 11, de la forme [image_width, image_height, pixel_size, focal, ppx, ppy, r1, r2, r3, t1, t2]
     */

    ifstream inFile;
    int sum = 0;
    std::vector<float> retour(11);

    inFile.open(file_path);

        if (!inFile) {
            cout << "Unable to open file";
            exit(1); // terminate with error
        }

        std::string str;
        while (std::getline(inFile, str)) {
            sum+=1;
            if (sum==2)
            {
                typedef std::vector<std::string> Tokens;
                    Tokens tokens;
                    boost::split( tokens, str, boost::is_any_of("\t") );
                    retour[0] = std::stof(tokens[0]);
                    retour[1] = std::stof(tokens[1]);
                    retour[2] = std::stof(tokens[2]);
                    retour[3] = std::stof(tokens[3]);
                    retour[4] = std::stof(tokens[4]);
                    retour[5] = std::stof(tokens[5]);
                    retour[6] = std::stof(tokens[6]);
                    retour[7] = std::stof(tokens[7]);
                    retour[8] = std::stof(tokens[8]);
                    retour[9] = std::stof(tokens[9]);
                    retour[10] = std::stof(tokens[10]);
            }

        }
        inFile.close();
        return retour;
}



double getResolution(int id_image, vector<double> centroid, const std::string ori_file_path, const std::string cali_file_path)
{/*
     * Calcule la résolution moyenne d'une image
     * Entrée : identifiant de l'image, coordonnées du centroid du nuage de points, chemins d'accès au fichier de calibration externe et interne
     * Sortie : resolution de l'image sous forme de double
     */

    vector<float> cali = getCalibration(cali_file_path);
    float pixel_size = cali[2];
    float focal = cali[3];
    vector<float> ori = getPosOri(id_image, ori_file_path);
    float h = sqrt(pow(centroid[0]-ori[0], 2) + pow(centroid[1]-ori[1], 2) + pow(centroid[2]-ori[2], 2));
    float res = (pixel_size * h)/focal;
    return res;

}



bool isInside(Vec2 point, double width, double height)
{
    /*
     * détermine si un point est a l'interieur d'un rectangle définit par sa hauteur et largeur.
     * Entrée : point sous forme de vecteurn largeur et hauteur du triangle
     * Sortie : booléen valant "true" si le point est à l'interieur du rectangle
     */
    if (point[0] <width)
    {
        if (point[1] <height)
        {
            return true;
        }
    }
    return false;
}



openMVG::image::RGBColor getRGB_TEMP(int id_image, std::string const image, double x, double y, double z, const std::string cali_file_path, const std::string ori_file_path)
{
    /*
     * Fonction temporaire utilisée pour tester la fonction fill_ortho de la pseudo-class Ortho, en attendant le débugage de la vraie fonction getRGB
     * La fonction getRGB devait initialement projeter un point 3d dans l'image pour trouver sa couleur
     * Entrée : mêmes paramètres que la vraie fonction getRGB
     * Sortie : couleur constante de type openMVG::image::RGBColor
     */
    return RGBColor(0,1,2);
}

//openMVG::image::RGBColor getRGB(int id_image, std::string const image, double x, double y, double z, const std::string cali_file_path, const std::string ori_file_path)
//{
//    /*
//     *      * * * NON FONCTIONNELLE * * *
//     * Calcule la couleur d'un point 3D tel qu'il est vu par une image en particulier
//     * Entrée : identifiant de l'image dans le repertoire, chemin de l'image servant à attribuer la couleur, coordonnées x, y et z du point,
//     *          chemins du fichier d'orientation interne et externe
//     * Sortie : couleur du point de type openMVG::image::RGBColor
//     */

//    // Récupération des différents paramètres de la calibration interne :
//    vector<float> cali = getCalibration(cali_file_path);
//    static double image_width = cali[0];
//    static double image_height = cali[1];
//    static double pixel_size = cali[2];
//    static double focal = cali[3];
//    static double ppx = cali[4];
//    static double ppy = cali[5];
//    static double r1 = cali[6];
//    static double r2 = cali[7];
//    static double r3 = cali[8];
//    static double t1 = cali[9];
//    static double t2 = cali[10];

//    vector<float> ori = getPosOri(id_image, ori_file_path);
//    static double Xcam = ori[0];
//    static double Ycam = ori[1];
//    static double Zcam = ori[2];
//    static double Yaw = ori[3];
//    static double Pitch = ori[4];
//    static double Roll = ori[5];


//    // Création d'un modèle de caméra avec distorsion
//    std::shared_ptr<IntrinsicBase>* intrinsic;
//    *intrinsic = std::make_shared<Pinhole_Intrinsic_Brown_T2> (image_width, image_height, focal, ppx, ppy, r1, r2, r3, t1 ,t2);

//    // Créer de la matrice de passage coord terrain - coord  image à partir des matrices de calibration K et de rotation & translation
//    // Instanciation des différentes matrices
//    Mat34 projection_matrix;
//    Mat3 camera_matrix_rotation;
//    camera_matrix_rotation << cos(Pitch)*sin(Yaw),  cos(Yaw)*sin(Pitch)*sin(Roll) - sin(Yaw)*cos(Roll), cos(Yaw)*sin(Pitch)*cos(Roll) + sin(Yaw)*sin(Roll),
//           cos(Pitch)*sin(Yaw), sin(Yaw)*sin(Pitch)*sin(Roll) + cos(Yaw)*cos(Roll), sin(Yaw)*sin(Pitch)*sin(Roll) - cos(Yaw)*cos(Roll),
//           -sin(Pitch), cos(Pitch)*sin(Roll), cos(Pitch)*cos(Roll);

//    Vec3 camera_projection_center;
//    camera_projection_center << Xcam, Ycam, Zcam;


//    openMVG::geometry::Pose3 image_pose(camera_matrix_rotation, camera_projection_center);


//    cameras::Pinhole_Intrinsic *pinhole_cam = dynamic_cast<cameras::Pinhole_Intrinsic *>(intrinsic);

//    openMVG::P_From_KRt( pinhole_cam->K() , image_pose.rotation() , image_pose.translation(), & projection_matrix);

//     // Lire image
//    openMVG::image::Image<openMVG::image::RGBColor> image_content;
//    std::string image_path = image;
//    openMVG::image::ReadImage(image_path.c_str(), &image_content);

//     // Project a point 3D to image 2D and get distorsion coordinates

//    Vec3 terrain_point(x,y,z);
//    Vec2 point_image = openMVG::Project(projection_matrix, terrain_point);     // project
//        if ( isInside(point_image , image_width , image_height ) )  // check if projected point is inside image
//        {
//            // get distorsed pixel
//            Vec2 point_image_with_d = pinhole_cam->get_d_pixel(point_image);
//            if ( isInside(point_image_with_d , image_width , image_height ) )
//           {
//                 // get RGB in image without interpolation
//                    openMVG::image::RGBColor color = image_content (std::round(point_image_with_d.y()) ,  std::round(point_image_with_d.x()));
//                    return color;
//            }
//        }
//    return RGBColor(0,1,2);
//}
