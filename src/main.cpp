#include "ortho.h"
#include "pointcloud.h"
#include "photos.h"

#include <iostream>
#include <algorithm>
#include <vector>
#include <memory>
#include <cstdlib>
#include <iostream>
#include <filesystem>

#include "laswriter.hpp"

#include <pcl/point_types.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "openMVG/image/image_io.hpp"
#include "openMVG/exif/exif_IO.hpp"
#include "openMVG/exif/exif_IO_EasyExif.hpp"
#include "openMVG/third_party/stlplus3/filesystemSimplified/file_system.hpp"

#include "openMVG/sfm/sfm.hpp"


//using namespace openMVG::exif;
using namespace openMVG;
using namespace openMVG::image;
using namespace std;

#include <string>
#include <limits.h>
#include <unistd.h>
#include <list>
#include <stdio.h>
#include <dirent.h>



int main(int argc,char* argv[])
{
    cout << argv[0] << endl;
    cout << "Make_Ortho project" << endl;
    if ( argc != 5 )
    {
        cout << "Mauvais nombre d'arguments";
        return 0;
    }

    cout << "You have entered " << argc
             << " arguments:" << "\n";

        for (int i = 0; i < argc; ++i)
            cout << argv[i] << "\n";

//___________________________________PATHS______________________________________________________
    const std::string your_las_file_path = argv[1];
    const std::string ImageRepository = argv[2];
    const std::string ImageCalibration = argv[3];
    const std::string ImageOrientation = argv[4];

//    const std::string your_las_file_path = "/home/anais/TEST1/mdLidar1000Dataset/LAS_UTM/20181101_DroneSN0290_FN0442_00.las";
//    const std::string ImageRepository = "/home/anais/TEST1/mdLidar1000Dataset/images";
//    const std::string ImageCalibration = "/home/anais/TEST1/mdLidar1000Dataset/InternalCalibration/InternalCalibration.txt";
//    const std::string ImageOrientation = "/home/anais/TEST1/mdLidar1000Dataset/ExternalOrientation/event1_eo_Mission_1.txt";

    const std::string pcdFile = "./pcd_file.pcd";
//    const std::string sImg = "/home/anais/TEST1/mdLidar1000Dataset/images/IMG_0000.jpg";






//_________________________Create PCD file_________________________________________________________
    las2pcd(your_las_file_path, pcdFile);
    cout <<"Convertion to PCD file done" << endl;


//_______________________Get Min, Max and centroid of the point cloud______________________________
    vector<double> MinMax = getMinMax(pcdFile);
//    for (std::size_t i = 0; i < MinMax.size (); ++i)
//        {
//            cout << MinMax[i] << endl;
//        }
    cout <<"Bounding box done" << endl;
    std::vector<double> centroid(3);
    centroid[0] = MinMax[3] + (MinMax[0] - MinMax[3])/2;
    centroid[1] = MinMax[4] + (MinMax[1] - MinMax[4])/2;
    centroid[2] = MinMax[5] + (MinMax[2] - MinMax[5])/2;
//    for (std::size_t i = 0; i < centroid.size (); ++i)
//        {
//            cout << centroid[i] << endl;
//        }
    cout <<"centroid done"<< endl;


//_______________________Fill Orthophoto_____________________________________________________
    double res = getResolution(3, centroid, ImageOrientation, ImageCalibration);
    cout <<"resolution calculated"<< endl;

    cout <<"filling the orthoimage ... "<< endl;
    fillOrtho(ImageRepository, pcdFile, res, MinMax, ImageCalibration, ImageOrientation);
    cout <<"orthoimage done "<< endl;

    return 0;
}
