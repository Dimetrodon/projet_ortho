#include "ortho.h"
#include "pointcloud.h"
#include "photos.h"

#include "iostream"
#include "fstream"
#include "string"
#include "cstdlib"


#include "openMVG/exif/exif_IO_EasyExif.hpp"

#include "openMVG/image/image_io.hpp"
#include "openMVG/image/image_container.hpp"
#include "openMVG/image/image_converter.hpp"
#include "openMVG/image/pixel_types.hpp"

#include "openMVG/multiview/solver_essential_kernel.hpp"
#include "openMVG/multiview/projection.hpp"
#include "openMVG/multiview/triangulation.hpp"
#include "openMVG/multiview/solver_resection_kernel.hpp"
#include "openMVG/multiview/solver_resection_p3p.hpp"

#include "openMVG/cameras/Camera_Pinhole_Radial.hpp"
#include "openMVG/cameras/Camera_undistort_image.hpp"

#include "openMVG/numeric/eigen_alias_definition.hpp"

#include "openMVG/multiview/test_data_sets.hpp"

#include "openMVG/sfm/sfm.hpp"

#include "openMVG/sfm/pipelines/sfm_robust_model_estimation.hpp"


using namespace openMVG;
using namespace openMVG::image;
using namespace openMVG::cameras;
using namespace openMVG::geometry;
using namespace openMVG::sfm;
using namespace std;

#include <string>
#include <list>
#include <iostream>
#include <memory>
#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <dirent.h>
#include <bits/stdc++.h>
#include <stdio.h>

void saveImg(Image<RGBColor> imaRGB, double resolution, vector<double> MinMax)
{
    const std::string filename = "./ortho.jpg";
    WriteImage(filename.c_str(), imaRGB);

   // Ecriture du fichier de géoréférencement :
    std::ofstream outfile ("./ortho.jpgw");

    outfile <<resolution<< std::endl;
    outfile <<0<< std::endl;
    outfile <<0<< std::endl;
    outfile <<- resolution<< std::endl;
    outfile <<MinMax[3]<< std::endl;
    outfile <<MinMax[1]<< std::endl;

    outfile.close();
}



std::list<std::string> repertoire(std::string Images_File)
{
    const char * c = Images_File.c_str();
    std::list<string> fichiers;
    DIR * rep = opendir(c);

        if (rep != NULL)
        {
            struct dirent * ent;

            while ((ent = readdir(rep)) != NULL)
            {
//                cout << ent->d_name <<endl;
                fichiers.push_back(ent->d_name);
            }

            closedir(rep);
        }
    return fichiers;
}

int mean(std::list<int> liste)
{
    /*
     * calcule la moyenne des nombres entiers compris dans une liste
     */

    int sum = 0;
    int n = 0;
    for (auto const& i: liste)
    {
        sum += i;
        n+=1;
    }
    double avg = sum / n;
    return int (avg);
}



void fillOrtho(const std::string Images_File, const std::string& pcdFile, double resolution, vector<double> MinMax, const std::string cali_file_path, const std::string ori_file_path)
{
    /*
     * Remplissage de l'orthophotgraphie.
     * La fonction commence par créer une orthoimage du taille définie par l'emprise du nuage et la resolution calculée,
     * puis pour chacun des pixels elle cherche leur couleur dans l'ensemble des images du repertoire d'image.
     *
     * Entrée : chemin du repertoire des images, chemin du nuage de points .pcd, resolution et vecteur MinMax issu de la fonction GetMinMax de la pseudo-classe Pointcloud
     * Sortie : Void. A la fin l'image est enregistrée, ainsi que son fichier de géoréférencement.
     */

    // Création du repertoire des images : liste des nom des images contenues dans le repertoire d'image
    std::list<std::string> rep = repertoire(Images_File);

    // Initialisation de la couleur et des listes servant à calculer la moyenne des couleurs
    openMVG::image::RGBColor couleur;
    std::list<openMVG::image::RGBColor> List_couleurs;
    std::list<int> R;
    std::list<int> V;
    std::list<int> B;

    int id_image = 0;

    // Coordonnées minimales du nuage
    double Xmin = MinMax[3];
    double Ymin = MinMax[4];

    // Calcul de la largeur et hauteur de l'ortho en fonction de l'emprise du nuage
    double width_ortho = MinMax[0] - MinMax[3];
    double height_ortho = MinMax[1] - MinMax[4];

    // Nombre de lignes et colonnes de l'ortho
    const int cl = int (width_ortho/resolution);
    const int ln = int (height_ortho/resolution);
//     std::cout << ln << " " << cl << "\n";

    // Creation de l'ortho vide
    Image<RGBColor> imaRGB(cl, ln);

//    for (int l = 0; l < ln; ++l)  // On parcourt l'ortho
//        {
//        for (int c = 0; c < cl; ++c)
//            {

//                std::vector<double> pixel(3); // On considère un pixel
//                pixel[0] = Xmin + c*resolution;  // Correspondance coordonnées dans l'ortho / coordonnées terrain
//                pixel[1] = Ymin + l*resolution;
//                vector<float> NearestNeigh = getNeighbor(pcdFile, pixel[0], pixel[1], 0);
//                pixel[2] = NearestNeigh[2];  // On considère que le z du pixel est le z de son plus proche voisin dans le nuage
//                if (NearestNeigh[3] > 1) // Si le pixel est trop loin du nuage / distance > 1m
//                {
//                    couleur = RGBColor(0,0,0); // ...le pixel est noir
//                }

//                else {
//                    int id_image = 0;
//                    for (auto const& i: rep) // On parcourt le repertoire des images
//                    {
//                        std::string const image_path = { Images_File +"/"+ i }; // chemin de l'image = chemin du repertoire + nom de l'image
//                        couleur = getRGB_TEMP(id_image, image_path, pixel[0], pixel[1], pixel[2], cali_file_path, ori_file_path); // On recupère la couleur de ce pixel dans l'image en question
//                        List_couleurs.push_back (couleur);
//                        R.push_back(int (couleur[0]));
//                        V.push_back(int (couleur[1]));
//                        B.push_back(int (couleur[2]));

//                        id_image +=1;
//                    }
//                    double red = mean(R);
//                    double ve = mean(V);
//                    double bl = mean(B);
//                    couleur = RGBColor(red, ve, bl);
//                    imaRGB(l,c) = couleur; // On attribue au pixel de l'ortho la moyenne des couleurs calculées
//                }
//            }
//        }

    saveImg(imaRGB, resolution, MinMax); // L'ortho remplie est enregistrée, ainsi qu'un fichier de géoréférencement
    const char * c = pcdFile.c_str();
    remove(c); // Suppréssion du nuage de points au format .pcd
}
